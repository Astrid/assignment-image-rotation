//
// Created by Kirill on 20.01.2022.
//

#include "../include/image.h"
#include "../include/error.h"
#include <stdio.h>
struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static enum read_status check_header(const struct bmp_header header) {
    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.biPlanes != 1) {
        return READ_INVALID_PLANES;
    }
    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }
    if (header.biCompression != 0) {
        return READ_INVALID_COMPRESSION;
    }
    if (header.biSize != 40 || header.bOffBits + header.biSizeImage != header.bfileSize) {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

static uint8_t padding_counter(struct image* const img) {
    return (img->width)%4;
}

struct bmp_header create_bmp_header(struct image* img) {
    struct bmp_header new_bmp_header = {
            .bfType = 0x4D42,
            .bfileSize = sizeof(struct bmp_header) +
                         img->height * img->width * sizeof(struct pixel) +
                         img->height * padding_counter(img),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage =  img->height * img->width * sizeof(struct pixel) + padding_counter(img) * img->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,

    };
    return new_bmp_header;
}



enum read_status from_bmp(FILE* in, struct image* img) {
    if (!in || !img) {
        return READ_ERROR;
    }
    struct bmp_header header;
    fread(&header, sizeof(struct bmp_header), 1, in);
    if (check_header(header) != READ_OK) {
        return check_header(header);
    }
//    if () {
//        printf("Can't read header");
//        return READ_HEADER_ERROR;
//    }

    img->width = header.biWidth;
    img->height = header.biHeight;
    uint8_t padding = padding_counter(img);
//    printf("padding is %u\n",padding);
    img->data = malloc(sizeof(struct pixel)*(img->width)*img->height);
    for (size_t i = 0; i<img->height; i++) {
        if(!fread(&(img->data[i*(img->width)]), sizeof(struct pixel), img->width, in)) {
            printf("Can't read pixels");
            return READ_PIXEL_ERROR;
        }
        fseek(in, padding, SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image* img) {
    if (!out || !img) {
        return WRITE_ERROR;
    }
    struct bmp_header result = create_bmp_header(img);
    fwrite(&result, sizeof(struct bmp_header), 1, out);

//    if (!) {
//        printf("Can't write header");
//        return WRITE_ERROR;
//    }
    uint8_t padding = padding_counter(img);
    const uint64_t zero = 0;
    for (size_t i = 0; i < img->height; i++) {
        if (!fwrite(&(img->data[i * img->width]), sizeof(struct pixel), img->width, out)
            || (padding && !fwrite(&zero, 1, padding, out))) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
