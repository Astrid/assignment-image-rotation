//
// Created by Kirill on 20.01.2022.
//

#include "../include/bmp.h"
#include "../include/image.h"
struct image rotate(struct image input_image) {
    struct image rotated_image = {.height = input_image.width, .width = input_image.height};
    //printf("width = %llu, height = %llu\n", rotated_image.width, rotated_image.height);
    rotated_image.data = malloc(sizeof(struct pixel)*rotated_image.width*rotated_image.height);
//    uint8_t cur = 0;
//    for (uint64_t i = 0; i < input_image.width; i++) {
//        for (uint64_t j = input_image.height; j > 0; j--) {
////            printf("b = %u, g = %u, r = %u\n", input_image.data[cur].b, input_image.data[cur].g, input_image.data[cur].r);
//            *(rotated_image.data+cur) = *(input_image.data+input_image.width*(j-1)+i);
//            cur++;
//        }
//    }
    for (uint64_t i = 0; i < rotated_image.width; i++) {
        for (uint64_t j = 0; j < rotated_image.height; j++) {
            *(rotated_image.data + j * rotated_image.width + (rotated_image.width - 1 - i)) =
                    *(input_image.data + i * rotated_image.height + j);
        }
    }
//    printf("size of pixel = %zu\n", sizeof(struct pixel));
//    printf("last elem of data = %u\n", (input_image.data+)->b);
    return rotated_image;
}
