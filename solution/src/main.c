#include "../include/bmp.h"
#include "../include/check.h"
#include <stdio.h>
#include <stdlib.h>
int main( int argc, char** argv ) {
    (void) argc; (void) argv; // suppress 'unused parameters' warning

    if (argc != 3) {
        printf("Wrong number of parameters");
        return 1;
    }
    if (check_in(argv[1]) != READ_OK) {
        return check_in(argv[1]);
    }
    FILE* f = fopen(argv[1], "rbe");
    if (f == NULL) {
        return 1;
    }
    struct image given;
    if (from_bmp(f, &given)) {
        return 1;
    }
//    printf("width = %llu, height = %llu\n", given.width, given.height);
    struct image rotated_image;
    rotated_image = rotate(given);
    if (check_in(argv[2]) != READ_OK) {
        return check_in(argv[2]);
    }
    FILE* f2 = fopen(argv[2], "wbe");
    if (f2 == NULL) {
        return 1;
    }
    if (to_bmp(f2, &rotated_image)) {
        return 1;
    }
    if (fclose(f) || fclose(f2)) {
        return 1;
    }
    free(given.data);
    free(rotated_image.data);
//    fwrite(f, sizeof(struct bmp_header), 1, f2);
    return 0;
}

