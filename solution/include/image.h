//
// Created by Kirill on 21.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_IMAGE_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
struct pixel __attribute__((packed)){
    uint8_t b, g, r;
};
struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image rotate(struct image input_image);
#endif //ASSIGNMENT_IMAGE_ROTATION_MASTER_IMAGE_H
