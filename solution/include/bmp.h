//
// Created by Kirill on 20.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_H

#include "error.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>
enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, struct image* img);
struct bmp_header;

#endif //ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_H
